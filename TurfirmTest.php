<?php

include 'Turfirm.php';
use PHPUnit\Framework\TestCase;

class TurfirmTest extends TestCase
{
    private $turik;

    protected function setUp() : void
    {
        $this->turik = new Turfirm();
        $this->turik->addBaza('Goroshek',"Гори",5000);
    }

    /**
     * @dataProvider TurDataProvider
     * @param $type
     * @param $b
     */
    public function test_Count($type,$b){
        $result=$this->turik->getBazCountByType($type);
        $this->assertEquals($b,$result);
    }

    public function TurDataProvider(){
        return array(
            array('Гори',3),
            array("Море",1),
            array("Зелений туризм",17)
        );
    }
    public function test_min(){
        $result=$this->turik->minBaza();
        $this->assertEquals("Дерево",$result);
    }
}
