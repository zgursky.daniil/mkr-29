<?php
include 'Baza.php';

class Turfirm
{
private $masa;

    /**
     * Turfirm constructor.
     * @param $masa
     */
    public function __construct()
    {
        $this->masa = array(
            new Baza('Дерево','Зелений туризм',2000),
            new Baza('Гора','Гори',3000),
            new Baza('Горочка','Гори',3000),
            new Baza('Волна','Мре',5000),
        );
    }

    public function addBaza($name,$type,$cost){
        $this->masa[] = new Baza($name,$type,$cost);
    }

    public function getBazCountByType($type){
        $i = 0;
        foreach ($this->masa as $basa){
            if($basa->getType() == $type){
                $i++;
            }
        }
        return $i;
    }

    public function minBaza(){
        $b=1000000;
        foreach($this->masa as $basa){
            $a = $basa->getCost();
            if($a<=$b){
            $b=$a;
            }
        }
        foreach ($this->masa as $basa){
            if($basa->getCost()==$b)
            echo $basa->getName();
        }
    }

}